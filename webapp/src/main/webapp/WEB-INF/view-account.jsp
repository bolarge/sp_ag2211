<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<sec:csrfMetaTags/>
<style>
<!--
#linkExportToXML,
        #uploadAccountXMLForm {
            display: none;
        }
-->
</style>

<div class="container">
<div style="margin-left: 400px;"><h5 style="color: red;"> ${error}</h5></div>
<div style="margin-left: 400px;"><h5 style="color: green;"> ${message}</h5></div>
  <h3>Hi <c:out value="${account.getAccountName()}"/></h3>
  <strong>Your Email</strong>: <c:out value="${account.getEmail()}"/><br> 
  <strong>Additional Info</strong>: <c:out value="${account.getAdditionalInfo()}"/><br>
 <img id="blah" style="margin-bottom: 5px;" src="${pageContext.servletContext.contextPath}/admin/getImage?accId=${account.getId()}" height="111px" width="111px"/>
  
  <!-- <div id="menu_xml" class="row form-group"> -->
  <form modelAttribute="account" action="uploadImage" method="post" enctype="multipart/form-data">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="file" multiple accept='image/*' style="margin-bottom: 5px;" id="imgInp" name="image"/>
     <input class="btn btn-success" type="submit"  value="Upload"/>
     <button type="button" class="btn btn-success" id="exportXMLButton">ExportXML</button>                      
     <button type="button" class="btn btn-success" id="importXMLButton">ImportXML</button>
    <input type="hidden" name="id" value=<c:out value="${account.getId()}"/>>
  </form>
  
<!-- XML Update id="uploadAccountXMLForm"-->
<a href="exportToXML" id="linkExportToXML">Click here to download file</a>
<form modelAttribute="account" id="uploadAccountXMLForm" enctype="multipart/form-data" method="post" action="uploadAccountXML">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input  id="fileXML" type="file" name="xmlFile">
    <input  id="importXMLFile" type="submit" value="Save">
    <input type="hidden" name="id" value=<c:out value="${account.getId()}"/>>
</form>
<!-- End XML Update -->
  
  <strong>Available data sets</strong>:
  <h3>Available DataSets for current account </h3>
  <div>
    <a type="button" class="btn btn-success" href="<c:url value="/admin/add-watchlist?id=${account.getId()}"/>">Add New watchlist</a>
    <a type="button" class="btn btn-primary"
             			href="<c:url value="/admin/search"/>">Search</a>
  </div>
  <hr size="4" color="gray"/>

  <table class="table table-striped">
		<thead>
    		<tr>
    			<th>Data Set Name</th>
    			<th>Data Set Id</th>
    			<th>Action</th>
    		</tr>
    	</thead>
    	<tbody>
			    <c:forEach var="theWatch" items="${watchLists}">
			      <tr>
			        <td>${theWatch.getWatchListName()}  </td>
			        <td>${theWatch.getWatchListId()}   </td>
			
			        <td><a type="button" class="btn btn-primary"
			               href="<c:url value="/admin/view-watchlist?watchListId=${theWatch.watchListId}&accountId=${account.getId()}"/>">View</a>
			            <a type="button" class="btn btn-primary"
			               href="<c:url value="/delete-watchlist?watchListId=${theWatch.watchListId}&accountId=${account.getId()}"/>">Delete</a>
			        </td>
			      </tr>
			
			    </c:forEach>
			    <tr>
			    
		</tbody>
  </table>
  <br>
  
</div>
<script type="text/javascript">

	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	
	        reader.onload = function (e) {
	            $('#blah').attr('src', e.target.result);
	        }
	
	        reader.readAsDataURL(input.files[0]);
	    }
	}
	
	$("#imgInp").change(function(){
	    readURL(this);
	});
	
	$("#exportXMLButton").on('click', function () {
        $("#linkExportToXML")[0].click();
    });

    $("#importXMLButton").on('click', function () {
        $("#fileXML").click();
    });

    $("#fileXML").on("change", function () {
        $("#importXMLFile").click();
    });
</script>

<%@ include file="common/footer.jspf" %>