package com.gjj.igden.model;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.gjj.igden.utils.EntityId;


@Entity
@Table(name = "account")
public class Account2 implements UserDetails, EntityId {

	private static final long serialVersionUID = -8703071584693304580L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "account_id")
    private Long id;
	
	@Column(name = "account_name", unique = true)
    private String accountName;
	
	@Column(unique = true)
    private String email;
	
    @Column(name = "additional_info")
    private String additionalInfo;
    private String password;
    
    @OneToMany(mappedBy = "account", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<WatchListDesc> descriptions;
    
    @Column(name = "creation_date")
    private Date creationDate;
    
    private boolean enabled;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "account_roles",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "role")})
    private Set<Role> roles = new HashSet<>();
    
    @Lob
   	@Basic(fetch = FetchType.LAZY)
   	@Column(name="avatar",length=20971520)
    private byte[] avatar;
    
    @Transient
    private String saltSource = "admin";
    
    public String getSaltSource() {
		return saltSource;
	}

	public Account2() {
    }

    public Account2(Long id) {
		this.id = id;
	}

	public Account2(String accountName, String email, String additionalInfo, String password,
                   List<WatchListDesc> descriptions, Date creationDate,byte[] avatar) {
        this.accountName = accountName;
        this.setEmail(email);
        this.additionalInfo = additionalInfo;
        this.password = password;
        this.descriptions = descriptions;
        this.creationDate = creationDate;
        this.avatar = avatar;
    }

    public Account2(Long id, String accountName, String email,
                   String additionalInfo, String password,
                   List<WatchListDesc> dataSets, Date creationDate) {
        this.id = id;
        this.accountName = accountName;
        this.setEmail(email);
        this.additionalInfo = additionalInfo;
        this.password = password;
        this.descriptions = dataSets;
        this.creationDate = creationDate;
    }

    public Account2(String accountName, String email,
                   String additionalInfo) {
        this.accountName = accountName;
        this.setEmail(email);
        this.additionalInfo = additionalInfo;
    }

    public Account2(Long id, String accountName, String email, String additionalInfo,
    		Date creationDate) {
        this.id = id;
        this.accountName = accountName;
        this.setEmail(email);
        this.additionalInfo = additionalInfo;
        this.creationDate = creationDate;
    }

/*  public Account(String accountName, String email, String additionalInfo,
                 List<WatchListDesc> dataSets) {
    this(null, accountName, email, additionalInfo, dataSets);
  }*/

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account2(Long accountId, String accountName, String email, String additionalInfo) {
        this.id = accountId; this.accountName = accountName; this.email = email; this.additionalInfo = additionalInfo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }


    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Transient
    public List<WatchListDesc> getAttachedWatchedLists() {
        return descriptions;
    }

    public void setDescriptions(List<WatchListDesc> descriptions) {
        this.descriptions = descriptions;
    }

    @Transient
    public List<WatchListDesc> getDataSets() {
        return descriptions;
    }

    public void setDataSets(List<WatchListDesc> dataSets) {
        this.descriptions = dataSets;
    }
    
    public List<WatchListDesc> getDescriptions() {
		return descriptions;
	}
    
	@Override
    public int hashCode() {
        return com.google.common.base.Objects.hashCode(getEmail(), accountName);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Account2 && ((Account2) obj).getEmail().equals(this.getEmail()) &&
                Objects.equals(((Account2) obj).accountName, this.accountName);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(" [id=");
        builder.append(id);
        builder.append(", accountName= ");
        builder.append(accountName);
        builder.append(", email= ");
        builder.append(getEmail());
        builder.append(", additionalInfo= ");
        builder.append(additionalInfo);
        builder.append(", data_containers Sets names= ");
        builder.append(descriptions);
        builder.append("]\n");
        return builder.toString();
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }


    @Override
    @Transient
    public String getUsername() {
        return getAccountName();
    }


    @Override
    @Transient
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }


    @Override
    @Transient
    public boolean isAccountNonExpired() {
        return isEnabled();
    }

    @Override
    @Transient
    public boolean isAccountNonLocked() {
        return isEnabled();
    }

    @Override
    @Transient
    public boolean isCredentialsNonExpired() {
        return isEnabled();
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public void addRole(Role role) {
    	this.roles.add(role);
    }

	public byte[] getAvatar() {
		return avatar;
	}

	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}
    
	
    
}
