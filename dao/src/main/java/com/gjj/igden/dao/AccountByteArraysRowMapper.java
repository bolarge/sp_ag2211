package com.gjj.igden.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class AccountByteArraysRowMapper implements RowMapper<byte[]> {
  public byte[] mapRow(ResultSet resultSet, int i) throws SQLException {
    return resultSet.getBytes("image");
  }
}
