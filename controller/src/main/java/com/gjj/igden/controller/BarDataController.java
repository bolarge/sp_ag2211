package com.gjj.igden.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gjj.igden.model.Bar;
import com.gjj.igden.service.barService.BarService;

@Controller
public class BarDataController {
	
	private static final Logger logger = LoggerFactory.getLogger(BarDataController.class);
	
  @Autowired
  private BarService service;

  @GetMapping("/admin/view-data")
  public String viewBarData(ModelMap model, @RequestParam String stockSymbol) {
	  logger.debug("Viewing bar data for stockSymbol::" + stockSymbol);
    Set<Bar> barList = service.getBarList(stockSymbol);
    model.addAttribute("barData", barList);
    return "view-data";
  }

  @GetMapping("/admin/search")
  public String searchGet1() {
	  logger.debug("Viewing bar data for stockSymbol::");
    return "search";
  }

  @GetMapping("/admin/ajax_search")
  public ResponseEntity<String> searchGet2(@RequestParam String searchParam) {
    List<String> tickets = service.searchTickersByChars(searchParam);
    String result = "";
    try {
		result = writeListToJsonArray(tickets);
		logger.debug("Ajax autocomplete search result::" + result);
	} catch (IOException e) {
		logger.error("Error in Ajax autocomplete search ::" + e);
		e.printStackTrace();
	}
    return new ResponseEntity<String>("{\"item\":"+result+"}", HttpStatus.OK) ;
  }
  
  @GetMapping("/admin/ajax_search2")
  public ResponseEntity<String> searchGet3(@RequestParam String searchParam) {
    List<String> tickets = service.searchTickersByChars3(searchParam);
    String result = "";
    try {
		result = writeListToJsonArray(tickets);
		logger.debug("Ajax autocomplete search result::" + result);
	} catch (IOException e) {
		logger.error("Error in Ajax autocomplete search ::" + e);
		e.printStackTrace();
	}
    return new ResponseEntity<String>("{\"item\":"+result+"}", HttpStatus.OK) ;
  }
  
  @GetMapping("/admin/DataController")
  public String searchGet(ModelMap model, @RequestParam("myText") String myText) {
	  logger.debug("Fetching bar list for symbol::" + myText);
    List<Bar> bars = service.getFullTickersObjectByChars(myText);
    model.addAttribute("THE_SEARCH_RESULT_LIST", bars);
    return "search";
  }

  @PostMapping("/admin/search")
  public String searchPost(ModelMap model, @RequestParam String stockSymbol) {
	  logger.debug("Fetching bar list for symbol::" + stockSymbol);
    Set<Bar> barList = service.getBarList(stockSymbol);
    model.addAttribute("barData", barList);
    return "view-data";
  }
  
  public String writeListToJsonArray(List<String> list) throws IOException {  
	    final OutputStream out = new ByteArrayOutputStream();
	    final ObjectMapper mapper = new ObjectMapper();

	    mapper.writeValue(out, list);
	    return out.toString();
	}
  
}
