package com.gjj.igden.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.gjj.igden.dao.daoUtil.DAOException;
import com.gjj.igden.dao.daoimpl.RoleDaoImpl;
import com.gjj.igden.model.Account;
import com.gjj.igden.model.Role;
import com.gjj.igden.service.accountService.AccountService;
import com.gjj.igden.service.serviceException.ServiceException;

@SessionAttributes("account") //included to keep track of object state for the implementation and support XML
@Controller
public class AccountController {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private AccountService service;

	@Autowired
	private RoleDaoImpl roleDaoImpl;

	@GetMapping("/admin/list-accounts")
	public String showAccounts(ModelMap model) {
		logger.debug("Reteriving list of Account");
		model.addAttribute("ACCOUNT_LIST", service.getAccountList());
		return "list-accounts";
	}

	@PostMapping("/admin/edit-account")
	public String putEditedAccountToDb(Account account, RedirectAttributes redirectAttributes) {
		logger.debug("Editing Account for id::" + account.getId());
		boolean editStatus = service.updateAccount(account);
		if (editStatus) {
			redirectAttributes.addAttribute("success", "true");
		} else {
			System.err.println("editing failed");
		}
		return "redirect:/admin/list-accounts";
	}

	@GetMapping("/add-account")
	public String createNewAccountGet(ModelMap model) {
		logger.debug("Creating an Account");
		model.addAttribute("accountRoles", roleDaoImpl.readAll());
		model.addAttribute("account", new Account());
		return "add-account";
	}

	@PostMapping("/processAddAccount1")
	public String createAccountPost1(@RequestParam(value = "username1", required = false) String username1) {
		logger.debug("Saving username ::in createAccountPost1::" + username1);
		Account account = new Account();
		account.setAccountName(username1);
		try {
			service.createAccount(account);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return "redirect:/list-accounts";
	}

	@PostMapping("/processAddAccount")
	public String createAccountPost2(@RequestParam(value = "username1", required = false) String username1) {
		logger.debug("Saving username::in createAccountPost2::" + username1);
		Account account = new Account();
		account.setAccountName(username1);
		try {
			service.createAccount(account);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return "redirect:/list-accounts";
	}

	@PostMapping("/add-account")
	public String createAccountPost(Account account, @RequestParam(value = "role", required = false) String role) {
		logger.debug("Saving role and account::in createAccountPost::" + account);
		account.addRole(roleDaoImpl.read(new Role(Long.parseLong(role))));
		try {
			service.createAccount(account);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DAOException e) {
			e.printStackTrace();
		}
		return "redirect:/admin/list-accounts";
	}

	@GetMapping("/delete-account")
	public String deleteAccount(@RequestParam Long id) {
		logger.debug("Deleting account for id::" + id);
		try {
			 service.delete(id);
		} catch (Exception e) {
			logger.error("Error while deleting account for id::" + id + e);
		}
			return "redirect:/admin/list-accounts";
	}

	@GetMapping("/admin/edit-account")
	public String getAccountToEditAndPopulateForm(ModelMap model, @RequestParam Long id) {
		logger.debug("Editing account for id:", + id);	
		Account account = service.retrieveAccount(id);
		logger.debug(String.format("Retrieving account for id::"),account);
		model.addAttribute("account", account);
		return "edit-account";
	}

	@GetMapping("/admin/getImage")
	@ResponseBody
	public void showImage(@RequestParam("accId") int accId , HttpServletResponse response )
			throws ServletException, IOException {
		logger.debug("Retrieving image for itemid",accId);
		byte[] buffer = service.getImage(accId);
		response.setContentType("image/jpg");
	    response.getOutputStream().write(buffer, 0, buffer.length);
	    response.getOutputStream();
	}

	@PostMapping("/admin/uploadImage") // new annotation since 4.3 todo make this new annotation everywhere
	public String setNewImage(ModelMap model, @RequestParam("image") MultipartFile imageFile, RedirectAttributes redirectAttributes,
			Account account) {
		Account account1 = service.retrieveAccount(account.getId());
		logger.debug("Uploading image for account" + account);
		if (imageFile.isEmpty()) {
			model.addAttribute("watchLists", account1.getAttachedWatchedLists());
			model.addAttribute("account", account1);
			model.addAttribute("error", "Please select file to upload'" + imageFile.getOriginalFilename() + "'");
			return "view-account";
		}
		
		if(!service.validateExtension(imageFile.getOriginalFilename())) {
			 model.addAttribute("error", "File type is not supported");
			 model.addAttribute("account", account1);
			 return "view-account";
		}
		try {
			byte[] bytes = imageFile.getBytes();
			InputStream imageConvertedToInputStream = new ByteArrayInputStream(bytes);
			service.setImage(account.getId(), imageConvertedToInputStream);
			model.addAttribute("watchLists", account1.getAttachedWatchedLists());
			model.addAttribute("account", account1);
			 model.addAttribute("message", "You successfully uploaded '" + imageFile.getOriginalFilename() + "'");
			/*redirectAttributes.addFlashAttribute("message",
					"You successfully uploaded '" + imageFile.getOriginalFilename() + "'");*/
			logger.debug("Image uploaded for account" + account);
		} catch (IOException e) {
			logger.error("Error in Image uploading for account" + account);
			e.printStackTrace();
		}
		
	        return "view-account";
	}

	@GetMapping("/uploadStatus")
	public String uploadStatus() {
		logger.debug("Image uploaded successfully");
		return "uploadStatus";
	}

	@GetMapping("/admin/view-account")
	public String viewAccount(ModelMap model, @RequestParam Long id) {
		logger.debug("Viewing account for id" + id);
		Account account = service.retrieveAccount(id);
		model.addAttribute("watchLists", account.getAttachedWatchedLists());
		model.addAttribute("account", account);
		return "view-account";
	}

	@RequestMapping(value = "/home")
	public String home() {
		logger.debug("Redirecting to home page");
		return "/other-jsp/home";
	}

	@RequestMapping(value = "/location")
	public String location() {
		logger.debug("Redirecting to location");
		return "/other-jsp/location";
	}
	
	@RequestMapping(value = "/admin/exportToXML")
    @ResponseBody
    public byte[] exportToXML(@SessionAttribute Account account, HttpServletResponse response) throws ServletException {
		logger.debug("Image uploaded for account" + account.toString());
        response.setContentType("application/xml");
        response.addHeader("Content-Disposition", "attachment; filename=account.xml");
        try {
            return service.parseToXML(account).getBytes();
        } catch (ServiceException e) {
            throw new ServletException(e.getMessage(), e);
        }
    }

	@PostMapping(value = "/admin/uploadAccountXML")
	 protected String uploadAccountXML(ModelMap model, @RequestParam("xmlFile") MultipartFile xmlFile, RedirectAttributes redirectAttributes,
				Account account) throws IOException, ServletException  {
		 try {
			 Account accXML = service.parseXMLToObject(multipartToFile(xmlFile));
			 logger.debug("XML file binded to account " + accXML);
			 service.updateAccount(accXML); 				 
			 model.addAttribute("message", "You successfully updated account  details '" + xmlFile.getOriginalFilename() + "'");
		 } catch (ServiceException e) {
	            throw new ServletException(e.getMessage(), e);	            
	     }
		return "view-account";
		//return "redirect:/admin/list-accounts";
    }
	
	public File multipartToFile(MultipartFile file) throws IOException {
	    File convFile = new File(file.getOriginalFilename());
	    convFile.createNewFile();
	    FileOutputStream fos = new FileOutputStream(convFile);
	    fos.write(file.getBytes());
	    fos.close();
	    return convFile;
	}
}
