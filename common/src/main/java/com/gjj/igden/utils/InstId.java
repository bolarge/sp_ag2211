package com.gjj.igden.utils;

import com.google.common.base.Objects;

/**
 * Instrument identifier
 */
public class InstId {
  private final static String SEPARATOR = "@";
  private String symbol;
  private String exchId;
  private Exchange exchange;

  public InstId(String instId) {
    validateInstId(instId);
  }

  public String getInstId() {
    return symbol + SEPARATOR + exchId;
  }
  public String getSymbol() {
    return symbol;
  }
  public String getExchId() {
    return exchId;
  }
  public Exchange getExchange() {
    return exchange;
  }

  private void validateInstId(String instId) {
    if (null != instId) {
      if (!(instId = instId.trim()).isEmpty()) {
        if (instId.contains(SEPARATOR)) {
          String[] tokens = instId.split(SEPARATOR);
          String symbol = tokens[0];
          String exchId = tokens[1];
          if (null != symbol && !(symbol = symbol.trim()).isEmpty() &&
              null != exchId && !(exchId = exchId.trim()).isEmpty()) {
            boolean validExchId = (exchId.equalsIgnoreCase("NASDAQ") || exchId.equalsIgnoreCase("NYSE"));
            if (validExchId) {
              this.symbol = symbol.toUpperCase();
              this.exchId = exchId.toUpperCase();
              if (exchId.compareTo("NASDAQ") == 0) {
                this.exchange = Exchange.NASDAQ;
              }
              if (exchId.compareTo("NYSE") == 0) {
                this.exchange = Exchange.NYSE;
              }
              return;
            }
          }
        }
      }
    }
    throw new IllegalArgumentException("Invalid InstId: " + instId);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(symbol, exchId);
  }

  @Override
  public boolean equals(Object object) {
    if (!(object instanceof InstId)) {
      return false;
    }
    InstId other = (InstId) object;
    if ((null == symbol && null != other.symbol) ||
        (null != symbol && symbol.compareTo(other.symbol) != 0) ||
        (null == exchId && null != other.exchId) ||
        (null != exchId && exchId.compareTo(other.exchId) != 0)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return symbol + SEPARATOR + exchId;
  }
}

