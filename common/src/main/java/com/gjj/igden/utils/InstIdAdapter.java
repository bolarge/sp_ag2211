package com.gjj.igden.utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by IntelliJ IDEA.
 *
 * @author saurav
 * @date 11/15/2017
 * @time 12:54 AM
 */
public class InstIdAdapter extends XmlAdapter<String, InstId> {
  public InstId unmarshal(String v) throws Exception {
    return new InstId(v);
  }
  public String marshal(InstId v) throws Exception {
    return v.toString();
  }
}