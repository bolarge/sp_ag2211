**Spring JPA Test Assignment**

##Task to include XML export & import feature
XMLImport issue is now resolved and functional. Concluding this took more time than i expected. Due to coding difference, I needed to understand more the implementation in the code bases a bit more. This is especially true for Social Network App, which mere copying and pasting will not address. Task is completed and Total commit is four.

Issue with XMLExport resolved. Left to resolve is XMLImport, target resolution time from now is 12 noon 25/11/2017. In  all my earlier estimate of four commits for the task will hold. Am aware of my inconsistencies with time in getting this task completed. There where stuffs to learn along the way and the code bases where quite different in style and implementation.

Testing brought out issues with my implementation of the XML functionality. Challenge with XML export is that it does not export the properties of the Account object in session. Challenge with ImportXML has to do with CRSF on the form, need to understand that more. As a result, may request to continue with the initial conclusion time of 6pm 24/11/2017 

Export XML and Import XML added to view. Next i  will be testing to ensure proper functioning. At this point my next commit will be my final. So I will be adjusting my time as i do not need to what i have estimated in my time extension request. My completion time will be  1.30pm on 24/11/2017.

Upgraded account entity to JAXB object and introduced logic at service layer to marshall Account objects. Had to read up JAXB as I am not familiar with it. This slowed me down and due to this may I request for additional time (6hrs) to cover up remaining 3 commits. Due date remains 24/11/2017 6pm

+XMLImport
+XMLExport
This task will require 4 commits in all to complete. Each feature will have 2 commits. Estimated time is 6 working hours. Task is due 24/11/2017 



Updated the Steps to Deploy.

Added two profile for test and development environments.
Test will initialize the database on every startup of server.
Development will only initialize the database at very first time.
Default profile is development.

Test cases completed!


Steps to Deploy the Application
---

*1*. Import project in to IDE.

*2*. Change database properties such as **user-name, password, driver, dialect or other properties** in following two files:
		 **'/config/src/main/resources/liquibase.properties'**
	and  **'/dao/src/test/resources/jdbcTemp.properties'**.
		**liquibase.properties**: this database is used as production database for the populating database using liquibase
		**jdbcTemp.properties**: this database is used for Junit test cases.

*3*. Create database-schema named as **mentioned in above properties files**.

*4*. Right click on main module: '/springjpa_test_assignment' and then execute 'Maven clean' command.

*5*. Rignt click on main module: '/springjpa_test_assignment' and then execute 'Maven install' command.

*6*. Again Right click on main module: '/springjpa_test_assignment' and then click on Maven -> Update Project -> Select All Modules -> select 'Force Update of Snapshots' -> OK

*7*. Add tomcat user role in tomcat-users.xml as:
        **(Note : This configuration is required to deploy the Application into Tomcat server using tomcat-maven plugins)**

		<role rolename="manager-gui"/>
  		<user password="s3cret" roles="manager-gui" username="tomcat"/>
	
*8*. Update above username and password in '/springjpa_test_assignment/pom.xml'.

			<plugin>
				<groupId>org.apache.tomcat.maven</groupId>
				<artifactId>tomcat7-maven-plugin</artifactId>
				<version>2.2</version>
				<configuration>
					<url>http://localhost:8080/manager/html</url>
					<server>TomcatServer</server>
					<path>/webapp</path>
					<username>tomcat</username>
					<password>s3cret</password>
				</configuration>
			</plugin>

*9*. Open terminal and navigate to project folder 'springjpa_test_assignment'. Execute the 

		command as: **'mvn tomcat7:run-war'** --default profile
		
		for test: **'mvn tomcat7:run-war -Ptest'**
		
		for development: **'mvn tomcat7:run-war -Pdev -Dmaven.test.skip=true'**
		
*10*. Application started at the url: **'http://localhost:8080/webapp/'**

*11*. Default user-name:**'FinBackTesting' and password:'123qwe'**